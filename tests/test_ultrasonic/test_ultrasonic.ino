#include <NewPing.h>

const int UltrasonicTriggerPin = 6;
const int UltrasonicEchoPin = 5;
const int MaxDistance = 200;
 
NewPing sonar(UltrasonicTriggerPin, UltrasonicEchoPin, MaxDistance);
 
void setup() {
  Serial.begin(9600);
}
 
void loop() {
float distance;
  delay(50);// esperar 50ms entre pings (29ms como minimo)
  Serial.print(sonar.ping_cm()); // obtener el valor en cm (0 = fuera de rango)
  Serial.println("cm");
}
